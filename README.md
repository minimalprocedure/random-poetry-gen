================================================================================
rp-gen, stupido generatore poetico basato su modelli di frasi
================================================================================

Usage: rp-gen [options] templates generated  
    -s, --signature sign             Author Signature  
    -q, --quantity num               Poetry quantity  
    -t, --type t                     HTML or Markdown file  
    -c, --css css                    HTML additional css  
    -h, --help                       Display this screen  
    
    
Il programma, legge tutti i file di testo inseriti nella cartella <templates> mischia le frasi estraendone sei in maniera casuale cercando di controllare numeri e punteggiatura spuria.  
Il file può essere salvato come markdown o HTML. In caso di HTML si può indicare un file di stylesheet da caricare.  



