[title: O dire dove.]

Fino a quando continuerà così?  
Il passato aumenta e il futuro diminuisce.  
 
Il tempo continua a scorrere?  
Le parole sono molto pericolose.  
 
Nei sogni cominciano le responsabilità  
possono diventare una vera e propria arma. [m:engine]  
