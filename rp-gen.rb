# -*- coding: utf-8 -*-

#################################################################################
# RANDOM POETRY GENERATOR (c) minimalprocedure <zairik@gmail.com>
# Version: 0.0.1 alpha
# Note: Based on a template folder.#
################################################################################

require 'kramdown'
require 'fileutils'
require 'optparse'

class String

  def to_slug(size = 128, sep = '-')
    unless self == '.' || self == '..' 
      chars = %r{[^[A-Za-z0-9]]|\b[aeiou]\b}i
      self.strip.gsub(chars, sep).squeeze(sep).gsub(%r{^-|-$},'').downcase
    else
      'dot'
    end
  end

end

module RandomPoetryGeneratorMod
  AUTHOR_SIGNATURE = "[m:engine]"
  TRAILING = "  " #markdown hard wrap
  TEMPLATES = "./templates"
  GENERATED = "./generated"
  QUANTITY = 1
  TYPE = :md
  EXTENSIONS = %w( txt md)

  MIN_WORDS = 4
  MAX_WORDS = MIN_WORDS * 3
end

class RandomPoetryGenerator

  include RandomPoetryGeneratorMod

  def initialize(tpath = TEMPLATES, gpath = GENERATED, options = {})
    Random.srand
    @signature = options[:signature].nil? ? AUTHOR_SIGNATURE : options[:signature]
    @quantity = options[:quantity].nil? ? QUANTITY : options[:quantity]
    @type = options[:type].nil? ? TYPE : options[:type].to_sym
    @css = options[:css]
    @tpath = tpath
    @gpath = gpath
    load_templates    
  end

  def load_templates
    templates = Dir.glob(File.join(@tpath, "*.{#{EXTENSIONS.join(',')}}"))
    @lines = templates.map {|template| File.open(template, 'r') {|f| f.readlines } }
    @lines.flatten!
    @lines.sort!       
    @lines = @lines.map {|line|
      unless line.match(/\[title:.*\]/) || line.strip.empty?
        line.downcase!
        line.gsub!(/^\d*\W\d*|^\d*\W|^\W|^\d*/, '') #numbers init line
        #line.gsub!(/^\d*\W\d*|^\d*\W|^\W|^\d*/, '') #numbers init line
        line.gsub!(/\[m.*\]/, '')
        line.gsub!(/<<|>>/, '')
        line.gsub!(/«|»/, '')
        line.strip!
        #line.gsub!(/\W$/, '')
        line.gsub!(/\./, '')
        line.squeeze!(" ")
        line.strip!
        words = line.split(' ')
        if words.size > MIN_WORDS && words.size < MAX_WORDS
          verses = []
          until words.size <= 0
            verses << words.slice!(0, Random.rand(MIN_WORDS..MAX_WORDS))
          end
          verses.map { |verse|      
            (verse.last.size > 2 ? verse : verse[0..2]).join(' ')
          }          
        end
      end
    }
    @lines.flatten!
    @lines.compact!
    @lines.uniq!
    @lines.shuffle!
  end
  
  ARTICLES = ['il', 'lo', 'la', 'i', 'gli', 'le', 'un', 'uno', 'una']

  def compone
    @poem = {
      :title => @lines[Random.rand(@lines.size)].capitalize.gsub(/\d*\W\d*$|\d*\W$|\W$|\d*$/, ''),
      :text => []
    }
    if @lines.size >= 9 
      @poem[:text] << (1..9).map { |num|
        case num
        when 3, 6
          ' '
        when 1,4,7
          @lines[num].capitalize + TRAILING
        when 2,5
          prev = @lines[num - 1].scan(/\W$/)
          line = @lines[num].gsub(/\W$/, '')
          regexp = Regexp.new("\s(#{ARTICLES.join('|')})$")          
          line.gsub!(regexp, '') if line.match(regexp)
          line += '.'
          (prev.empty? || prev[0] == ',' || prev[0] == ';' ? line : line.capitalize) + TRAILING
        when 8
          @lines[num].gsub(/\W$/, '') + ". #{@signature}" + TRAILING
        end
      }
    else
      @poem[:text] = []
    end
    @poem[:text].flatten!
    @poem[:text].compact!
  end

  def html
    compone
    title = "#{ @poem[:title].capitalize }"
    css = %(<link href="#{@css.to_s}" rel="stylesheet" crossorigin='anonymous'>) if @css
    text = Kramdown::Document.new(@poem[:text].join("\n")).to_html
    """
      <html>
      <head>
      <link href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css' 
      rel='stylesheet' 
      integrity='sha256-MfvZlkHCEqatNoGiOXveE8FIwMzZg4W85qfrfIFBfYc=sha512-dTfge/zgoMYpP7QbHy4gWMEGsbsdZeCXz7irItjcC3sPUFtf0kuFbDz/ixG7ArTxmDjLXDmezHubeNikyKGVyQ==' 
      crossorigin='anonymous'>
      #{ css }
      <title>#{ title }</title>
      </head>

      <body>
      <div class='container'>
        <h1>#{ title }</h1><br>
        <div class='poetry'>
        #{ text }
        </div>
      </div>
      <body>
      <html>
    """
  end

  def md
    compone
    "[title: #{@poem[:title]}.]\n\n#{@poem[:text].join("\n")}"
  end

  def save!(t = :md)
    text = case t
           when :md
             md
           when :html
             html
           else
             t = :md
             md
           end
    FileUtils.mkdir_p(@gpath)
    fname = File.join(@gpath, "#{@poem[:title].to_slug}.#{t.to_s}")
    puts "GENERATED: #{fname}"
    File.open(fname, 'w+') { |f| f.puts(text) }
  end

  def generate!
    @quantity.times.each {|poetry|
      save!(@type)
      @lines.shuffle!
    }
  end

  private :load_templates, :compone, :html, :md, :save!
  public :generate!
end

if __FILE__ == $0
  #r = RandomPoetryGenerator.new "/home/nissl/Documenti/Nissl/writing/trusting_nonsense/Texts"
  #r.save!

  b_opt = {}
  optparse = OptionParser.new { |opts|
    opts.banner = "Usage: rp-gen [options] templates generated"
    
    opts.on( '-s', '--signature sign', 'Author Signature' ) do |s|
      b_opt[:signature] = s.to_s
    end

    opts.on( '-q', '--quantity num', 'Poetry quantity' ) do |q|
      b_opt[:quantity] = q.to_i
    end

    opts.on( '-t', '--type t', 'HTML or Markdown file' ) do |t|
      b_opt[:type] = t.to_sym
    end

    opts.on( '-c', '--css css', 'HTML additional css' ) do |c|
      b_opt[:css] = c.to_s
    end
    
    opts.on( '-h', '--help', 'Display this screen' ) do
      puts opts
      exit
    end
    
  }
  optparse.parse!
  
  case ARGV.size
  when 0
    puts optparse.banner
  when 1
    gen = RandomPoetryGenerator.new(ARGV[0], RandomPoetryGeneratorMod::GENERATED, b_opt)
    gen.generate!
  when 2
    gen = RandomPoetryGenerator.new(ARGV[0], ARGV[1], b_opt)
    gen.generate!
  else
    puts optparse.banner
  end
  
end
